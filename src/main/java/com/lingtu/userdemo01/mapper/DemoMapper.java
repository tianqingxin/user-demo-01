package com.lingtu.userdemo01.mapper;

import java.util.List;
import java.util.Map;

public interface DemoMapper {

    List<Map<String,Object>> getUserList();
}

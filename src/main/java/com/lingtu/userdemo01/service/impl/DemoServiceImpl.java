package com.lingtu.userdemo01.service.impl;

import com.lingtu.userdemo01.mapper.DemoMapper;
import com.lingtu.userdemo01.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class DemoServiceImpl implements DemoService {
    @Autowired
    private DemoMapper demoMapper;
    @Override
    public List<Map<String, Object>> getUserList() {
        return demoMapper.getUserList();
    }
}

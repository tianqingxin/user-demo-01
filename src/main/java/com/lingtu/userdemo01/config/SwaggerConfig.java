//package com.lingtu.userdemo01.config;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import springfox.documentation.builders.ApiInfoBuilder;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.builders.RequestHandlerSelectors;
//import springfox.documentation.service.ApiInfo;
//import springfox.documentation.service.Contact;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spring.web.plugins.Docket;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;
//
//@Configuration
//@EnableSwagger2
//public class SwaggerConfig {
//    @Bean
//    public Docket createRestApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .apiInfo(apiInfo())
//                .select()
//                .apis(RequestHandlerSelectors.basePackage("com.lingtu"))
//                //basePage就是具体要扫描的包，有swagger注解的方法就生成到文档里面去
//                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
//                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
//                .paths(PathSelectors.any())
//                .build();
//    }
//    private ApiInfo apiInfo() {
//        return new ApiInfoBuilder()
//                .title("用户系统api")
//                .description("用户系统接口文档说明")
//                .contact(new Contact("灵图软件",
//                        "http://www.lingtu.com",
//                        "demo@lingtu.com"))
//                .version("1.0")
//                .build();
//    }
////    @Bean
////    UiConfiguration uiConfig() {
////        return new UiConfiguration(null, "list", "alpha", "schema",
////                UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS, false, true, 60000L);
////    }
//}

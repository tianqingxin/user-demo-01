package com.lingtu.userdemo01;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.lingtu.userdemo01")
public class UserDemo01Application {

    public static void main(String[] args) {
        SpringApplication.run(UserDemo01Application.class, args);
    }

}

package com.lingtu.userdemo01.service.impl;

import com.lingtu.userdemo01.service.DemoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DemoServiceImplTest {
    @Autowired
    private DemoServiceImpl demoService;

    @Test
    void getUserList() {
        List<Map<String, Object>> userList = demoService.getUserList();
        assertNull(userList);
    }
}
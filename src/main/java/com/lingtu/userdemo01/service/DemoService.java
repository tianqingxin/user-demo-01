package com.lingtu.userdemo01.service;

import java.util.List;
import java.util.Map;

public interface DemoService {
    List<Map<String,Object>> getUserList();
}

package com.lingtu.userdemo01.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;
@SpringBootTest
class DemoMapperTest {
    @Autowired
    private DemoMapper demoMapper;
    @Test
    void getUserList() {
        List<Map<String, Object>> userList = demoMapper.getUserList();
//        assertNull(userList);
        System.out.println(userList.toString());
    }
}
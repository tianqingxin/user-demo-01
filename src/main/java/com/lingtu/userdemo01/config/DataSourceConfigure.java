package com.lingtu.userdemo01.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration// 配置数据源
public class DataSourceConfigure {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource") // 数据源的自动配置的前缀
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }
} 

